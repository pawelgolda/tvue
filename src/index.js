#!/usr/bin/env node

const path = require('path')
const fs = require('fs')
const chalk = require('chalk')
const Handlebars = require('handlebars')
const log = console.log
const cwd = process.cwd()
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv

const componentName = argv._[0]
if (componentName === undefined) {
  log(chalk.red('✘ Provide component name'))
  process.exit()
}

let templateType = 'standard'
if (argv.vuetify) {
  templateType = 'vuetify'
}
if (argv.vue3) {
  templateType = 'vue3'
}

const vue = getTemplate('vue', templateType)
const js = getTemplate('js', templateType)
const scss = getTemplate('scss', templateType)

if (createDirectory()) {
  vue && saveFile('vue', vue)
  js && saveFile('js', js)
  scss && saveFile('scss', scss)
}

// Functions

function getTemplate (name, templateType) {
  const templatePath = path.join(__dirname, `/template/${templateType}/${name}.handlebars`)
  if (!fs.existsSync(templatePath)) {
    return false
  }
  const templateData = fs.readFileSync(templatePath, {
    encoding: 'utf-8'
  })
  return Handlebars.compile(templateData)({
    componentName
  })
}

function createDirectory () {
  const directory = path.join(cwd, componentName)
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory)
    log(chalk.green(`✔ Component directory ${componentName} created.`))
    return true
  }
  log(chalk.red(`✘ Component directory ${componentName} already exists!`))
  return false
}

function saveFile (ext, content) {
  const file = path.join(cwd, componentName, `${componentName}.${ext}`)
  if (!fs.existsSync(file)) {
    fs.writeFileSync(file, content)
    log(chalk.green(`✔ Component file ${componentName}/${componentName}.${ext} created.`))
    return true
  }
  log(chalk.red(`✘ Component file ${componentName}/${componentName}.${ext} already exists!`))
  return false
}
